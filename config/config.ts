/*
 * @Descripttion:
 * @version:
 * @Author: ZmSama
 * @Date: 2022-02-23 23:35:59
 */
import { defineConfig } from 'umi';
import routes from './routes';
export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  antd: {},
  layout: {
    name: '德业APS',
    // locale: true,
    navTheme: 'dark',
    layout: 'side',
    contentWidth: 'Fluid',
    headerHeight: 48,
    primaryColor: '#1890ff',
  },
  routes,
  fastRefresh: {},
  dva: {
    immer: true,
  },
});
