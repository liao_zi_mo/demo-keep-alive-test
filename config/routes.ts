/*
 * @Descripttion:
 * @version:
 * @Author: ZmSama
 * @Date: 2022-02-24 09:35:43
 */
export default [
  {
    exact: true,
    path: '/',
    redirect: '/basic-data/customer',
  },
  {
    path: '/basic-data',
    name: '基础数据',
    routes: [
      {
        exact: true,
        path: '/basic-data/customer',
        component: '@/pages/basic-data/customer/index',
        name: '客户',
      },
      {
        path: '/basic-data/material',
        component: '@/pages/basic-data/material/index',
        name: '物料',
        access: 'SYSTEM',
      },
      {
        exact: true,
        path: '/basic-data/workShop',
        name: '车间',
        component: '@/pages/basic-data/workShop/index',
      },
    ],
  },
];
