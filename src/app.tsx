import KeepAliveTabs from './components/KeepAliveTabs';
/*
 * @Descripttion:
 * @version:
 * @Author: ZmSama
 * @Date: 2022-02-24 09:55:07
 */
export const layout = () => ({
  headerRender: () => <KeepAliveTabs />,
});
