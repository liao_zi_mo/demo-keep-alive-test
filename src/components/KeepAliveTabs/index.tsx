/*
 * @Descripttion:
 * @version:
 * @Author: ZmSama
 * @Date: 2022-02-24 09:58:44
 */
import { message, Tabs } from 'antd';
import { history, useAliveController, useLocation } from 'umi';
import { AliveScope } from 'react-activation';
const { TabPane } = Tabs;
export default function index() {
  const { getCachingNodes, dropScope } = useAliveController();
  const cachingNodes = getCachingNodes();
  const location = useLocation();

  // 点击tab时的操作,跳转路由即可
  const tabClickHandler = (key: string) => {
    history.push({
      pathname: key,
    });
  };

  // 关闭操作，最后一项不能关闭
  const tabEditHanlder = (key: any) => {
    if (cachingNodes.length > 1) {
      console.log(location.pathname, key);
    } else {
      message.error('最后一项不能关闭');
    }

    // dropScope(key);
  };

  return (
    <AliveScope>
      <Tabs
        type="editable-card"
        hideAdd
        onTabClick={tabClickHandler}
        onEdit={tabEditHanlder}
        tabBarExtraContent={<div>这里放用户信息，退出登陆等操作</div>}
      >
        {cachingNodes.map((item) => {
          return <TabPane tab={item.label} key={item.name} />;
        })}
      </Tabs>
    </AliveScope>
  );
}
