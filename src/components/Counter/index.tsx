/*
 * @Descripttion:
 * @version:
 * @Author: ZmSama
 * @Date: 2022-02-24 13:46:45
 */
import { useState } from 'react';

export default function Counter() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <button onClick={() => setCount((count) => count - 1)}>-</button>
      <span style={{ padding: '0 6px' }}>count: {count}</span>
      <button onClick={() => setCount((count) => count + 1)}>+</button>
    </div>
  );
}
