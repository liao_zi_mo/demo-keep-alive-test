/*
 * @Description:这个文件是用来做权限的
 * @Autor: ZmSama
 * @Date: 2021-08-10 15:38:05
 */
/**
 * @see https://umijs.org/zh-CN/plugins/plugin-access
 * */
export default function access() {
  return {
    SYSTEM: true,
  };
  // let userInfo = localStorage.getItem('userInfo');
  // if (userInfo) {
  //   const { access } = JSON.parse(userInfo);
  //   return access;
  // } else {
  //   return {
  //     // 系统设置
  //     SYSTEM: false,
  //     SYSTEM_ORG_MANAGER: false, //组织管理
  //     SYSTEM_ORG_MANAGER_ADD: false, //新增组织
  //     SYSTEM_ORG_MANAGER_BATCH_DELETE: false, //批量删除组织
  //     SYSTEM_ORG_MANAGER_DELETE: false, //删除组织
  //     SYSTEM_ORG_MANAGER_EDIT: false, //编辑组织
  //     SYSTEM_ROLE_PERSSION: false, //角色和权限
  //     SYSTEM_ROLE_PERSSION_ADD: false, //添加角色
  //     SYSTEM_ROLE_PERSSION_EDIT: false, //编辑角色
  //     SYSTEM_ROLE_PERSSION_DELETE: false, //删除角色
  //     SYSTEM_ROLE_PERSSION_BATCH_DELETE: false, //批量删除角色
  //     SYSTEM_USER_MANAGER: false, //用户管理
  //     SYSTEM_USER_MANAGER_ADD: false, //新增用户
  //     SYSTEM_USER_MANAGER_BATCH_DELETE: false, //批量删除用户
  //     SYSTEM_USER_MANAGER_DELETE: false, //删除用户
  //     SYSTEM_USER_MANAGER_DESIGN_ROLE: false, //分派角色
  //     SYSTEM_USER_MANAGER_EDIT: false, //编辑用户
  //     SYSTEM_OPERATE_LOG: false, //操作日志
  //     // 基础数据
  //     BASIC: false,
  //     BASIC_CUSTOMER: false, //客户
  //     BASIC_CUSTOMER_EDIT: false, //编辑客户
  //     BASIC_CUSTOMER_TOOGLE: false, //停/启用客户
  //     BASIC_CUSTOMER_ADD: false, //新增客户
  //     BASIC_CUSTOMER_IMPORT: false, //导入客户
  //     BASIC_CUSTOMER_DELETE: false, //删除客户
  //     BASIC_CUSTOMER_BATCH_DELETE: false, //批量删除客户
  //     BASIC_MATERIAL: false, //物料
  //     BASIC_MATERIAL_ADD: false, //新增物料
  //     BASIC_MATERIAL_DELETE: false, //删除物料
  //     BASIC_MATERIAL_EDIT: false, //编辑物料
  //     BASIC_MATERIAL_BATCH_DELETE: false, //批量删除物料
  //     BASIC_MATERIAL_IMPORT: false, //导入物料
  //     BASIC_MATERIAL_SYNC_ERP: false, //同步erp
  //     BASIC_MATERIAL_TOOGLE: false, //停启用物料
  //     BASIC_WORKSHOP: false, //车间
  //     BASIC_WORKSHOP_ADD: false, //新增车间
  //     BASIC_WORKSHOP_EDIT: false, //编辑车间
  //     BASIC_WORKSHOP_DELETE: false, //删除车间
  //     BASIC_WORKSHOP_IMPORT: false, //导入
  //     BASIC_WORKSHOP_TOOGLE: false, //停/启用车间
  //     BASIC_SHIFT: false, //班次
  //     BASIC_SHIFT_ADD: false, //新增班次
  //     BASIC_SHIFT_EDIT: false, //编辑班次
  //     BASIC_SHIFT_DELETE: false, //删除班次
  //     BASIC_SHIFT_TOOGLE: false, //停/启用班次
  //     BASIC_CALENDAR_RULE: false, //日历规则
  //     BASIC_CALENDAR_RULE_ADD: false, //新增日历规则
  //     BASIC_CALENDAR_RULE_EDIT: false, //编辑日历规则
  //     BASIC_CALENDAR_RULE_DELETE: false, //删除日历规则
  //     BASIC_CALENDAR_RULE_TOOGLE: false, //停/启用日历规则
  //     BASIC_RESOURCE: false, //资源
  //     BASIC_RESOURCE_ADD: false, //新增资源
  //     BASIC_RESOURCE_EDIT: false, //编辑资源
  //     BASIC_RESOURCE_TOGGLE: false, //停/启用资源
  //     BASIC_RESOURCE_DELETE: false, //删除资源
  //     BASIC_RESOURCE_IMPORT: false, //导入资源
  //     BASIC_RESOURCE_GROUP: false, //资源组
  //     BASIC_RESOURCE_GROUP_ADD: false, //新增资源组织
  //     BASIC_RESOURCE_GROUP_EDIT: false, //编辑资源组
  //     BASIC_RESOURCE_GROUP_DELETE: false, //删除资源组
  //     BASIC_RESOURCE_GROUP_TOGGLE: false, //停/启用资源组
  //     BASIC_RESOURCE_GROUP_IMPORT: false, //导入资源组
  //     BASIC_RESOURCE_WORK_CALENDAR: false, //资源工作日历
  //     BASIC_RESOURCE_WORK_CALENDAR_BATCH_JUDGE: false, //批量调整日历规则
  //     BASIC_RESOURCE_WORK_CALENDAR_JUDGE: false, //调整日历规则
  //     BASIC_CAPACITY: false, //产能
  //     BASIC_CAPACITY_ADD: false, //新增产能
  //     BASIC_CAPACITY_EDIT: false, //编辑产能
  //     BASIC_CAPACITY_DELETE: false, //删除产能
  //     BASIC_CAPACITY_TOGGLE: false, //停/启用产能
  //     BASIC_CAPACITY_IMPORT: false, //导入产能
  //     BASIC_PERSONNEL_PLAN: false, //人员投入计划
  //     BASIC_PERSONNEL_PLAN_ADD: false, //新增人员投入计划
  //     BASIC_PERSONNEL_PLAN_EDIT: false, //编辑人员计划
  //     BASIC_PERSONNEL_PLAN_DELETE: false, //删除人员计划
  //     BASIC_PERSONNEL_PLAN_IMPORT: false, //导入人员计划
  //     BASIC_DEPARTMENT: false, //部门
  //     BASIC_DEPARTMENT_ADD: false, //新增部门
  //     BASIC_DEPARTMENT_EDIT: false, //编辑部门
  //     BASIC_DEPARTMENT_DELETE: false, //删除部门
  //     BASIC_DEPARTMENT_BATCH_DELETE: false, //批量删除部门
  //     BASIC_DEPARTMENT_TOOGLE: false, //停/启用部门
  //     BASIC_DEPARTMENT_IMPORT: false, //导入部门
  //     ORDER: false, //订单管理
  //     ORDER_DEMAND: false, //需求订单
  //     ORDER_DEMAND_ADD: false, //新增需求
  //     ORDER_DEMAND_EDIT: false, //编辑需求
  //     ORDER_DEMAND_CLOSE: false, //关闭需求
  //     ORDER_DEMAND_IMPORT: false, //导入
  //     ORDER_DEMAND_CALCULATION: false, //需求展算
  //     ORDER_DEMAND_GET_ERP: false, //获取erp数据
  //     ORDER_DEMAND_DELETE: false, //删除需求订单
  //   };
  // }
}
