/*
 * @Descripttion:
 * @version:
 * @Author: ZmSama
 * @Date: 2022-02-24 16:13:34
 */
import { Effect, Reducer } from 'umi';

export interface AliveTabsListState {
  count: number;
}
export interface IndexModelType {
  namespace: 'aliveTabList';
  state: AliveTabsListState;
  effects: {
    query: Effect;
  };
  reducers: {
    changeName: Reducer;
  };
}
const TabModel: IndexModelType = {
  namespace: 'aliveTabList',
  state: {
    count: 0,
  },
  reducers: {
    changeName(state) {
      state.count = state.count + 1;
    },
  },
  effects: {
    *query() {},
  },
};
export default TabModel;
