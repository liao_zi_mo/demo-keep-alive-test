/*
 * @Descripttion:
 * @version:
 * @Author: ZmSama
 * @Date: 2022-02-24 16:54:53
 */
import { FC } from 'react';
import {
  IndexModelState,
  ConnectProps,
  Loading,
  connect,
  KeepAlive,
} from 'umi';
interface PageProps extends ConnectProps {
  index: IndexModelState;
  loading: boolean;
}

const IndexPage: FC<PageProps> = (props) => {
  return (
    <KeepAlive
      name="/basic-data/workShop"
      saveScrollPosition="screen"
      label="车间"
    >
      <div>Hello</div>
    </KeepAlive>
  );
};

function mapStateToProps(state: any, loading: boolean) {
  return { name: state.workShop, loading };
}
export default connect(mapStateToProps)(IndexPage);
