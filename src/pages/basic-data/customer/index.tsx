/*
 * @Descripttion:
 * @version:
 * @Author: ZmSama
 * @Date: 2022-02-23 23:21:36
 */
import { Loading, connect, AliveTabsListState, ConnectProps } from 'umi';
import { FC, useEffect } from 'react';
import { KeepAlive } from 'react-activation';
import { Button } from 'antd';

interface PageProps extends ConnectProps {
  aliveTabList: AliveTabsListState;
  loading: boolean;
}
const Customer: FC<PageProps> = ({ aliveTabList, dispatch }) => {
  const { count } = aliveTabList;

  useEffect(() => {
    console.log(count);
  }, [count]);
  return (
    <div>
      <div>{count}</div>
      <Button
        onClick={() => {
          dispatch!({
            type: 'aliveTabList/changeName',
          });
        }}
      >
        点击我
      </Button>
    </div>
  );
};

export default connect(
  ({
    aliveTabList,
    loading,
  }: {
    aliveTabList: AliveTabsListState;
    loading: Loading;
  }) => ({
    aliveTabList,
    loading: loading.models.aliveTabList,
  }),
)((props) => (
  <KeepAlive
    name="/basic-data/customer"
    saveScrollPosition="screen"
    label="客户"
  >
    <Customer {...props} />
  </KeepAlive>
));
