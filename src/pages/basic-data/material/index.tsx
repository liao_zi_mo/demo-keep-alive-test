/*
 * @Descripttion:
 * @version:
 * @Author: ZmSama
 * @Date: 2022-02-23 23:22:20
 */
import { KeepAlive } from 'react-activation';

import Counter from '@/components/Counter';

function Home() {
  return (
    <div>
      <h1>Home</h1>
      <Counter />
      {Array(100)
        .fill('')
        .map((item, idx) => (
          <div key={idx}>Home Item {idx + 1}</div>
        ))}
    </div>
  );
}

export default () => (
  <KeepAlive
    name="/basic-data/material"
    saveScrollPosition="screen"
    label="物料"
  >
    <Home />
  </KeepAlive>
);
